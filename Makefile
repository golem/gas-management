IP  = 192.168.2.86
IP  = 10.42.0.81
user = golem

base := $(shell basename $(CURDIR))

all: transfer remote-build remote-run

transfer:
	ssh $(user)@$(IP) 'mkdir $(base) -p'
	scp -r ./RP/* $(user)@$(IP):~/$(base)

remote-build:
	ssh $(user)@$(IP) 'cd $(base) && make main'

remote-run:
	#ssh $(user)@$(IP) 'cd $(base) && python3 $(base).py'
	ssh $(user)@$(IP) 'cd $(base) && python3 analog_out_test.py'

remote-clean:
	ssh $(user)@$(IP) 'cd $(base) && make clean'

nuke:
	#removes remote foler
	#for moments when you remotely fuck it up
	ssh $(user)@$(IP) 'rm -rf $(base)'

ssh:
	ssh $(user)@$(IP)