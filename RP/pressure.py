class FakeSharedVariable:
	def __init__(self, filename="pressure.txt"):
		self.filename = filename
		self.value = 20e-3

	def get(self):
		return self.value

	def set(self, new):
		self.value = new