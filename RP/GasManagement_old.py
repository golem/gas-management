import AnalogOut
import time

from pressure import FakeSharedVariable


pressure = FakeSharedVariable()

out=AnalogOut.output()


def proportional(target, pressure, k=-1e1):
	p = pressure.get()

	d_p = p-target
	return k*d_p


pressure.set(15e-3)

target=20e-3

for i in range(10):
	out.add(proportional(target, pressure))
	print(proportional(target, pressure))
	time.sleep(0.2)

pressure.set(30e-3)

out.set(0)
for i in range(10):
	proportional(target, pressure)
	time.sleep(0.2)

pressure.set(20)


for i in range(10):
	proportional(target, pressure)
	time.sleep(0.2)



del out