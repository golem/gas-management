/* Red Pitaya C API example Generating continuous signal
 * This application generates a specific signal */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "redpitaya/rp.h"

int main(int argc, char **argv){

	/* Print error, if rp_Init() function failed */
	if(rp_Init() != RP_OK){
		fprintf(stderr, "Rp api init failed!\n");
	}

	/* Generator reset */
	rp_GenReset();


	//100V Power supply relay
	int unsigned pin_power;
	int unsigned pin_output;
    pin_power = RP_DIO1_P;
    pin_output = RP_DIO2_P;
	rp_DpinSetDirection (pin_power, RP_OUT);
	rp_DpinSetDirection (pin_output, RP_OUT);



	//turn on the power supply
    rp_DpinSetState(pin_power, RP_HIGH);


	/* Generating frequency */
	rp_GenFreq(RP_CH_1, 0);

	/* Generating wave form */
	rp_GenWaveform(RP_CH_1, RP_WAVEFORM_DC);


	char *wait = "bagr";
	size_t l = 0;



	/* Enable channel */
	rp_GenOutEnable(RP_CH_1);


    while(strncmp(wait, "stop", 4) != 0){
    	double voltage = atof(wait);

		rp_GenAmp(RP_CH_1, voltage);
		rp_GenOffset(RP_CH_1, 0);


		//rp_GenOutEnable(RP_CH_1);

        getline(&wait, &l, stdin);
    }

    printf("switching off \n");

    //HV relay
    rp_DpinSetState(pin_power, RP_LOW);





	/* Releasing resources */
	rp_Release();

	return 0;
}