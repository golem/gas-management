from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize


extensions = [
    Extension(
        name="redpitaya",
        sources=["redpitaya.pyx"],
        include_dirs=["/opt/redpitaya/include"]
    )
]

setup(
    name=("redpitaya"),
    ext_modules=cythonize(extensions)
)
